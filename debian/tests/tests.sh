#!/bin/sh

set -e

cd "$AUTOPKGTEST_TMP"

echo "Copyright (C)        2012-2014" >  testfile_basic
copyright-update -t testfile_basic > output_test
current_year=$(date +%Y)

if grep -q "Would change 2014 => $current_year" output_test; then
    echo "Testing change year is OK"
else
    echo "Testing change year failed"
    exit 1
fi

copyright-update testfile_basic

if grep -q "Copyright (C)        2012-$current_year" testfile_basic; then
    echo "Testing change year in file is OK"
else
    echo "Testing change year in file failed"
    exit 1
fi

echo "Copyright     (C)              2012-2014" >  testfile_format_whitespace
copyright-update testfile_format_whitespace

if grep -q "Copyright     (C)              2012-$current_year" testfile_format_whitespace; then
    echo "Testing with extra whitespaces is OK"
else
    echo "Testing with extra whitespaces failed"
    exit 1
fi

echo " " >  testfile_empty
copyright-update -d testfile_empty > output_empty

if grep -q "WARN: empty file" output_empty; then
    echo "Check empty file is OK"
else
    echo "Check empty file failed"
    exit 1
fi

echo "all superficial tests have passed"
exit 0
